 const ResponseCodes = {
  OK: 200,
  CREATED: 201,
  BAD_REQUEST: 400,
  UNAUTHORIZED: 401,
  FORBIDDEN: 403,
  NOT_FOUND: 404,
};

 const JWT = {
  expiresIn: '24h',
};
const saltRounds = 10;
 const Port = 1337
 module.exports = {
  Port,
  JWT,
  ResponseCodes,
  saltRounds
};
