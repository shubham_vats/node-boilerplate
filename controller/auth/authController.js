const {validateSignup, validateLogin} = require('../../helper/validation/user-validation')
const User = require('../../model/User')
const bcrypt = require('bcrypt')
const Token = require('../../helper/token')
const {ResponseCodes, saltRounds} = require('../../constant/constant')
const { updateOne } = require('../../model/User')
module.exports = {
  signup:async (req, res)=>{
    let response = {}
    console.log("req.bodu", req.body);
    let validate = await validateSignup(req.body)
    if (validate.error) {
      response.status = ResponseCodes.BAD_REQUEST;
      response.error = validate.errorMessage
      return res.status(ResponseCodes.BAD_REQUEST).json(response)
    }
    let data = {
      email:req.body.email,
      firstName: req.body.firstName,
      lastName:req.body.lastName,
      password:req.body.password
    }
    data.password = await bcrypt.hash(data.password, saltRounds)
    let user = await User.create(data).catch(error=>{
      response.status = ResponseCodes.BAD_REQUEST;
      response.error = error
      return res.status(ResponseCodes.BAD_REQUEST).json(response)
    })
    if (!user) {
      response.status = ResponseCodes.BAD_REQUEST;
      response.error = "No user created"
      return res.status(ResponseCodes.BAD_REQUEST).json(response)
    }
    response.status = ResponseCodes.CREATED;
    response.message = "Users created successfully"
    response.data = user
    return res.status(ResponseCodes.CREATED).json(response)
  },
  login: async (req, res) => {
    let response = {}
    let validate = await validateLogin(req.body)
    if (validate.error) {
      response.status = ResponseCodes.BAD_REQUEST;
      response.error = validate.errorMessage
      return res.status(ResponseCodes.BAD_REQUEST).json(response)
    }
    let user = await User.findOne({ email: req.body.email});
    if (!user) {
      
      response.status = ResponseCodes.BAD_REQUEST;
      response.error = "Invalid Email"
      return res.status(ResponseCodes.BAD_REQUEST).json(response)
    }
    let isPwdMatch = await bcrypt.compare(req.body.password, user.password);
    if (!isPwdMatch) {
      response.status = ResponseCodes.BAD_REQUEST;
      response.error = "Invalid Password"
      return res.status(ResponseCodes.BAD_REQUEST).json(response)
    }
    let token = await Token.generateToken(user)
    let updateToken = await User.updateOne({_id:user._id},{token:token}).catch(error=>{
      response.status = ResponseCodes.BAD_REQUEST
      response.error = "Some error occured"
      return res.status(ResponseCodes.BAD_REQUEST).json(response)

    })
    response.status = ResponseCodes.OK
    response.message = "Logged in successfully"
    response.data = updateToken
    return res.status(ResponseCodes.OK).json(response)



  
    return res.status(200).json({message:"login done"});
  }

}