const { JWT } = require("../constant/constant");
const jwt = require('jsonwebtoken');
const User = require('../model/User')
module.exports = {
  generateToken:async(data)=>{
    try {
      console.log("i am here",data, JWT, process.env.JWT_KEY);
      
      let userData = data;
      userData.id = userData._id;
      console.log("value of userdata", userData);
      let token = await jwt.sign(
        userData.toJSON(),
        process.env.JWT_KEY,
        JWT
      );
      console.log("value of token", token);
      return token;
    } catch (err) {
      console.log("err",err);
      return false;
    }
  },
  verifyToken: (req)=>{
    let result = {};
    if (req.header(AuthHeader)) {
      // if one exists, attempt to get the header data
      let token = req.header(AuthHeader).split('Bearer ')[1];
      // if there's nothing after "Bearer", no go
      if (!token) {
        result.status = false;
        result.err =
          'Unable to find the authentication token in the request header';
        return result;
      }
      // if there is something, attempt to parse it as a JWT token
      return jwt.verify(token, process.env.JWT_KEY, async (err, payload) => {
        // console.log("error",err);
        // console.log("payload",payload);

        if (err || !payload.id) {
          result.status = false;
          result.err = 'Unable to verify the authentication token';
          return result;
        }
        sails.log.info(payload.id);
        let user = await User.findById(payload.id);
        if (!user) {
          result.status = false;
          result.err = 'Invalid user';
          return result;
        }
        // if it got this far, everything checks out, success
        result.status = true;
        result.user = user;
        return result;
      });
    }
    result.status = false;
    result.err =
      'Unable to find the authentication token in the request header';
    return result;
  }
}