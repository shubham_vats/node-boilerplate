module.exports = {
  validateSignup: (data)=>{
    console.log("data", data);
    if (!data.email) {
      return({
        success:false,
        error:true,
        errorMessage:"Email required"
      })
    } if (!data.firstName) {
      return({
        success:false,
        error:true,
        errorMessage:"First name required"
      })
    }
    if (!data.lastName) {
      return({
        success:false,
        error:true,
        errorMessage:"Last name required"
      })
    } if (!data.password) {
      return({
        success:false,
        error:true,
        errorMessage:"Password required"
      })
    }
    return({
      success:true,
      error:false,
    })
  },
  validateLogin: (data)=>{
    console.log("data", data);
    if (!data.email) {
      return({
        success:false,
        error:true,
        errorMessage:"Email required"
      })
    }if (!data.password) {
      return({
        success:false,
        error:true,
        errorMessage:"Password required"
      })
    }
    return({
      success:true,
      error:false,
    })
  }
}