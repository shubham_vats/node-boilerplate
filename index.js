var express = require('express');
var app = express();
const bodyParser = require("body-parser");
const mongoose = require("mongoose");
const {Port} = require("./constant/constant")
const methodOverride = require("method-override");

require('dotenv').config()
const authRoutes = require('./routes/auth/authenticationRoute')
const userRoutes = require('./routes/user/userRoute')
app.use(bodyParser.urlencoded({ extended: false }))
 
// parse application/json
app.use(bodyParser.json())
app.use(methodOverride("_method"));


mongoose.connect(process.env.DATABASE_URL)

app.use(authRoutes)
app.use(userRoutes)
console.log("port that we have", Port, process.env.DATABASE_URL)
app.get('/',(req, res)=>{
  res.send(`Example app listening on Port ${Port}!`)
})

app.listen(Port, function () {
  console.log(`Example app listening on Port ${Port}!`);
});