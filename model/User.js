var mongoose = require("mongoose");
var UserSchema= mongoose.Schema({
    email:String,
    firstName:String,
    lastName:String,
    password:String,
    token:String  
});
module.exports=mongoose.model("User",UserSchema);